import pyautogui
import cv2
import numpy as np
import os
import time
import json
import datetime
scale_percent = 100 # percent of original size
delay = 1
img = pyautogui.screenshot()

img = np.array(img) 
# Convert RGB to BGR 
img = img[:, :, ::-1].copy() 

#crop
'''
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
# resize image
img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
'''
r = cv2.selectROI(img)
r1 = cv2.selectROI(img)
i = 0
cv2.destroyAllWindows()
f = open('result.csv','w')
# logs = open('logs.txt','w')

while(1):
  try:
    i += 1
    start = time.time()
    img = pyautogui.screenshot()
    img = np.array(img) 
    # Convert RGB to BGR 
    img = img[:, :, ::-1].copy() 
    imCrop = img[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
    color_crop = img[int(r1[1]):int(r1[1]+r[3]), int(r1[0]):int(r1[0]+r1[2])]
    cv2.imwrite("crop.jpg",imCrop)
    data = os.popen('curl -X POST "http://14.177.239.164:8001/ocr?CameraID=hunghx" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" -F "fileb=@crop.jpg;type=image/jpeg"').read()
    
    # logs.write(data + "\n")
    data = json.loads(str(data)[1:-5])
    res = data['result']
    
    if np.min(color_crop[:,:,0])<100:
      color = "red"
      # cv2.imwrite("red/"+str(res)+"_"+str(i)+".jpg", imCrop)
    else:
      color = "green"
      # cv2.imwrite("green/"+str(res)+"_"+str(i)+".jpg", imCrop)
    
    f.write("{},{},{}\n".format(datetime.datetime.now(),res,color))
    print("{}\t{}\t{}\n".format(datetime.datetime.now(),res,color))
    
    api_time = time.time() - start
    if api_time <= 1.0:
      time.sleep(1.0 - api_time)
  except:
    pass